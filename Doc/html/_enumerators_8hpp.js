var _enumerators_8hpp =
[
    [ "Animations", "_enumerators_8hpp.html#a58885e4b13a43077e3f5e47f2802873d", [
      [ "running", "_enumerators_8hpp.html#a58885e4b13a43077e3f5e47f2802873dab514bba77fe136c3a3b6f56b818f7b0c", null ],
      [ "jumping", "_enumerators_8hpp.html#a58885e4b13a43077e3f5e47f2802873dad2d3bad5b048938b81e2d343cafe886a", null ],
      [ "idle", "_enumerators_8hpp.html#a58885e4b13a43077e3f5e47f2802873da0e9a37114c0e458d28d52f06ec0f2242", null ]
    ] ],
    [ "ComponentType", "_enumerators_8hpp.html#a81f78fc173dedefe5a049c0aa3eed2c0", [
      [ "E_RendererComponent", "_enumerators_8hpp.html#a81f78fc173dedefe5a049c0aa3eed2c0af17fd9531c70dabd51d0accaca638721", null ],
      [ "E_BoxCollider2D", "_enumerators_8hpp.html#a81f78fc173dedefe5a049c0aa3eed2c0aec5efccb2f3ec204f308b7f4a2f4a2e3", null ],
      [ "E_PhysicsBody", "_enumerators_8hpp.html#a81f78fc173dedefe5a049c0aa3eed2c0a0a5ed940c51def86c4f912aaa867b09e", null ],
      [ "E_PlayerController", "_enumerators_8hpp.html#a81f78fc173dedefe5a049c0aa3eed2c0a5070f5f6336247d6f1fb55afedce7548", null ],
      [ "E_Animator", "_enumerators_8hpp.html#a81f78fc173dedefe5a049c0aa3eed2c0aceb4c5d88d35111af6f3b2cd8b2c040f", null ],
      [ "E_EnemyController", "_enumerators_8hpp.html#a81f78fc173dedefe5a049c0aa3eed2c0a72b4e593d9fa3cfc539ed0dabbb06ea6", null ],
      [ "E_Projectile", "_enumerators_8hpp.html#a81f78fc173dedefe5a049c0aa3eed2c0ad9ab451dbdaf8a49a83dbfb3a52388a9", null ]
    ] ],
    [ "GameState", "_enumerators_8hpp.html#a7899b65f1ea0f655e4bbf8d2a5714285", [
      [ "Uninitialized", "_enumerators_8hpp.html#a7899b65f1ea0f655e4bbf8d2a5714285a254bd1cf3c287ac4eb3d47320b1c92b6", null ],
      [ "ShowingMenu", "_enumerators_8hpp.html#a7899b65f1ea0f655e4bbf8d2a5714285a6340fc438fdd77124db864c9849ed4ae", null ],
      [ "ShowingAboutUs", "_enumerators_8hpp.html#a7899b65f1ea0f655e4bbf8d2a5714285a520621dd5264566c6d3554290c13986f", null ],
      [ "Playing", "_enumerators_8hpp.html#a7899b65f1ea0f655e4bbf8d2a5714285a63fb97da872bff3b2c623de28fdae93b", null ],
      [ "Exiting", "_enumerators_8hpp.html#a7899b65f1ea0f655e4bbf8d2a5714285aefad2513b4485534ba620ac9699849ea", null ]
    ] ],
    [ "MenuResult", "_enumerators_8hpp.html#ae6d3f527628d524904dec419a9cf3200", [
      [ "play", "_enumerators_8hpp.html#ae6d3f527628d524904dec419a9cf3200add7770619d252a3e8aadf3b86b3e5633", null ],
      [ "aboutUs", "_enumerators_8hpp.html#ae6d3f527628d524904dec419a9cf3200a589858e9c0727293adf6834255f7faf7", null ],
      [ "exitGame", "_enumerators_8hpp.html#ae6d3f527628d524904dec419a9cf3200affb25e94f15dab44efe7d7992c6bf790", null ],
      [ "none", "_enumerators_8hpp.html#ae6d3f527628d524904dec419a9cf3200ab7e4e0120a041dbe6528b050c04269e0", null ]
    ] ]
];