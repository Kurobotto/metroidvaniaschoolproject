#include "TilesetManager.hpp"

TilesetManager* TilesetManager::_instance = nullptr;

TilesetManager::~TilesetManager()
{
    for(int i = 0; i < _tileSets.size();i++)
    {
        delete _tileSets[i];
    }
}

sf::Sprite* TilesetManager::getSpriteFromGid(const int gid, const int tileSize)
{
    sf::Sprite* sprite;

    //Loop backward on all the tilesets
    for(int i = _tileSets.size()-1;i>=0;i--)
    {
        //If the tileset have a first gid > to the searched gid, it means that he hold the texture
        if(gid >= _tileSets[i]->getFirstGid())
        {
            //Get the index of the texture in the tileset
            int index = gid - _tileSets[i]->getFirstGid();
            int nbCol = _textures[i].getSize().x/tileSize;
            int nbRow = _textures[i].getSize().y/tileSize;

            //Get the x and y pos of the texture
            int rectPosX = ((int)(index/nbCol))*tileSize;
            int rectPosY = (index%nbCol)*tileSize;

            //Set the sprite to the correct texture
            sf::IntRect rect = sf::IntRect(rectPosY,rectPosX,tileSize,tileSize);
            sprite = new sf::Sprite(_textures[i],rect);
            break;
        }
    }

    return sprite;
}

void TilesetManager::addTileset(Tileset* tileset)
{
    _tileSets.push_back(tileset);

    sf::Texture texture;

    //Directly load the texture
    if(texture.loadFromFile(tileset->getPath()))
    {
        _textures.push_back(texture);
    }
    else
    {
        std::cout << "Tileset couldn't load is texture " << tileset->getPath() << std::endl;
    }
}
