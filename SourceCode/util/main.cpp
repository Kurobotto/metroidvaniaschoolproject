#include "../modele/GameLogic/Game.hpp"
#include "../modele/Level/Level.hpp"
#include "Serialization/LevelSerialization.hpp"
#include "TilesetManager.hpp"

//Major is the major version of the game, should only change with enormous update (imagine expansion).
//Pass to 1 when the game is first released.
const std::string& major = "0";
//Minor represent a minor update to the game. It should change when adding a few little things,
//but nothing big.
const std::string& minor = "2";
//Patch is a patch of the game. It should change every time we are pushing an update fixing bugs,mistakes,...
//There is no need to change it until we go in 1.0.0
const std::string& patch = "0";

const std::string& windowTitle = "Metroidvania v"+major+"."+minor+"."+patch;
const int bitsPerPixel = 32;

int main()
{
    //Before launching the game, we should access the user preferences and get the wanted width and height.
    Game* game = Game::getInstance();

    game->start(1280,720,bitsPerPixel,windowTitle);

    delete TilesetManager::getInstance();
    delete game;

    return 0;
}
