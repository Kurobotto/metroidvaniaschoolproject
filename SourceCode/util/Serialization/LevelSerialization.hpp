//! The level loader.
/*! Allow to load the given level from it's json file. */
#ifndef LEVELSERIALIZATION_HPP_INCLUDED
#define LEVELSERIALIZATION_HPP_INCLUDED

#include "../../modele/Level/Level.hpp"
#include "../../modele/Level/Layer.hpp"
#include "../Include/Jsoncpp/json/json.h"
#include <iostream>
#include <fstream>

class LevelSerialization
{
public:
    //! The level loader.
    /*! Allow to load the given level from it's json file. */
    static Level loadLevelFromFile(std::string path);
};

#endif // LEVELSERIALIZATION_HPP_INCLUDED
