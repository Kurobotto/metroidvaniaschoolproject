//! The gameobject loader.
/*! Allow to load all the json object from the given folder. */
#ifndef GOBSERIALIZATION_HPP_INCLUDED
#define GOBSERIALIZATION_HPP_INCLUDED

#include "../../modele/ComponentSystem/GameObject/GameObject.hpp"
#include "../Include/Jsoncpp/json/json.h"
#include <iostream>
#include <fstream>

class GobSerialization
{
public:
    //! The gameobject loader.
    /*! Allow to load all the json object from the given folder. */
    static GameObject* loadGobFromFile(std::string path);
};

#endif // GOBSERIALIZATION_HPP_INCLUDED
