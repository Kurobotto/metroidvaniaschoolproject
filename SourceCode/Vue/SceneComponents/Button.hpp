//! Represent a button with a label on it.
/*!
    Simply represent a classic button.
    The code to handle the click should be extern to the class.
*/
#ifndef BUTTON_HPP_INCLUDED
#define BUTTON_HPP_INCLUDED

#include <string>
#include "SFML/Window.hpp"
#include "SFML/Graphics.hpp"


#define BUTTON_WIDTH 200
#define BUTTON_HEIGHT 100

class Button
{
public:
    //! Constructor
    /*! Create the button. */
    Button(const int x, const int y, const int width, const int height, const std::string& imagePath, const std::string& text);
    //! Draw the button
    /*! Draw both the sprite and the text that make the button. */
    void draw(sf::RenderWindow& window);
    //! Getter for the rect
    /*! Return to rect of the button. Used to check if we are clicking on it. */
    sf::IntRect* getRect() {return &_rect;};
    //! Setter of the position
    /*! Set the position of the rect, the sprite and the text. */
    void setPos(const int x, const int y);
private:
    //! The button rect
    /*! The button rect represent the bounds of the button.\nIt is used to check if we are clicking on the button.*/
    sf::IntRect _rect;
    //! The sprite texture
    /*! The texture that is rendered by the sprite. */
    sf::Texture _texture;
    //! The sprite
    /*! What is shown on the screen, represent the background of our button. */
    sf::Sprite _sprite;
    //! The text font
    /*! The font used by the button's text. */
    sf::Font _font;
    //! The text
    /*! The text that is shown above the button. */
    sf::Text _text;
};

#endif // BUTTON_HPP_INCLUDED
