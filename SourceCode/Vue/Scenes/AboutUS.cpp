#include "AboutUs.h"


void AboutUs::initializeFrame(sf::RenderWindow& window)
{
    //Create the back button and set its position
    _returnButton = new Button(0,0,BUTTON_WIDTH,BUTTON_HEIGHT,"Ressources/img/button.png","Back");
    _returnButton->setPos(window.getSize().x/2-BUTTON_WIDTH/2,window.getSize().y/2-BUTTON_HEIGHT/2+125);

    //Load the font then create the credit text
    if(_CreditFont.loadFromFile("Ressources/font/arial.ttf"))
    {
        _CreditText = sf::Text("This game was made by Nicolas Viseur & Ahmad Mohamad. \n Students of the HELHa Campus Mons,\n in the context of the course of Mr. V. Altares.",_CreditFont,30);
        _CreditText.setPosition(window.getSize().x/3-BUTTON_WIDTH/2,window.getSize().y/2-BUTTON_HEIGHT/2-200);
        _CreditText.setFillColor(sf::Color::White);
    }
    else
    {
        std::cout << "couldn't load font" << std::endl;
    }
};


void AboutUs::executeFrame(sf::RenderWindow& window, const sf::Time& deltaTime)
{
    //Draw the button and the text, then display them
    _returnButton->draw(window);
    window.draw(_CreditText);

    window.display();

    //While we are waiting for the user to click the button ...
     while(true)
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            switch(event.type)
            {
            case sf::Event::MouseButtonPressed:
                {
                    //If the users have clicked the button, return to the main menu
                    bool clicked = handleClick(event.mouseButton.x,event.mouseButton.y);
                    if(clicked)
                    {
                        return;
                    }
                    break;
                }
            case sf::Event::Closed:
                //Close the game when quitting
                Game::getInstance()->changeState(GameState::Exiting);
                break;
            case sf::Event::Resized:
                //Resize the window and the view when needed, then redraw the scene
                sf::View playerView;
                playerView.reset(sf::FloatRect(0,0,window.getSize().x,window.getSize().y));
                window.setView(playerView);
                window.clear(sf::Color(0,128,128));

                _CreditText.setPosition(window.getSize().x/3-BUTTON_WIDTH/2,window.getSize().y/2-BUTTON_HEIGHT/2-200);
                _returnButton->setPos(window.getSize().x/2-BUTTON_WIDTH/2,window.getSize().y/2-BUTTON_HEIGHT/2+125);


                _returnButton->draw(window);
                window.draw(_CreditText);
                window.display();
                break;
            }

        }
    }

}



bool AboutUs::handleClick(const int x,const int y)
{
    //Return true if the button is clicked
    if(_returnButton->getRect()->intersects(sf::IntRect(x,y,1,1)))
    {
      Game::getInstance()->changeState(GameState::ShowingMenu);
      return true;
    }

    return false;
}

AboutUs::~AboutUs()
{
    delete _returnButton;
}


