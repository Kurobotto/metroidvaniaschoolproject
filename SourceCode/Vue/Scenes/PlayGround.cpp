#include "PlayGround.hpp"
#include <iostream>

void resizeView(const sf::RenderWindow& window,sf::View& view)
{
    //Resize the view to fit the screen
    float aspectRatio = float (window.getSize().x) / float (window.getSize().y);
    view.setSize(VIEW_HEIGHT * aspectRatio, VIEW_HEIGHT );
}

void PlayGround::initializeFrame(sf::RenderWindow& window)
{
    //Load the level and the gameobjects
    this->initLevel("Ressources/Levels/Level01.json");
    //Get the player renderer
    this->renderer = (RendererComponent*)(this->_player->getComponentOfType(E_RendererComponent));
    //Get the player controller and set it's projectile
    PlayerController* playerController = (PlayerController*) (this->_player->getComponentOfType(E_PlayerController));
    playerController->setProj(this->_projectile);

    //Start all the gameobjects
    for (int i = 0; i < _gameObjects.size(); i++)
    {
        _gameObjects[i]->start();
    }
}

void PlayGround::executeFrame(sf::RenderWindow& window, const sf::Time& deltaTime)
{
    //Handle the events
    sf::Event event;
    while (window.pollEvent(event))
    {
        switch(event.type)
        {
        case sf::Event::Closed:
            Game::getInstance()->changeState(GameState::Exiting);
            return;
            break;
        case sf::Event::Resized:
            resizeView(window,playerView);
            break;
        }
    }

    sf::Sprite* s;

    //If the enemy is deactivated, it means we won
    if(!this->_enemy->isActivated()){
        playerView.reset(sf::FloatRect(0,0,window.getSize().x,window.getSize().y));
        window.setView(playerView);
        Game::getInstance()->changeState(GameState::ShowingMenu);
        CollisionSystem::getInstance()->reset();
        return;
    }

    //Update the gameobjects
    for (int i = 0; i < _gameObjects.size(); i++)
    {
        if(_gameObjects[i]->isActivated())
        {
            s = _gameObjects[i]->update(deltaTime);
            if(s != nullptr)
            {
                window.draw(*s);
            }
        }
    }

    //Move the view accordingly to the player
    playerView.setCenter(this->renderer->getPosition().x+70,this->renderer->getPosition().y);
    resizeView(window,playerView);
    playerView.zoom(0.4df);
    window.setView(playerView);
}

void PlayGround::initLevel(const std::string& path)
{
    //Load the level from the json file
    this->_level = LevelSerialization::loadLevelFromFile(path);

    const int layerCount = _level.getLayersNumber();
    const int tileSize = _level.getTileSize();

    //Transform the level object into gameobjects
    for(int i=0;i<layerCount;i++)
    {
        const std::vector<Tile*>& tiles = _level.getLayerByIndex(i).getTiles();

        for(int j=0;j<tiles.size();j++)
        {
            GameObject* tile = new GameObject("tile"+i*j);

            RendererComponent* renderer = (RendererComponent*)tile->addComponentOfType(ComponentType::E_RendererComponent);

            if(renderer != nullptr)
            {
                renderer->setSprite(*TilesetManager::getInstance()->getSpriteFromGid(tiles[j]->getGid(),tileSize),tileSize,tileSize);
                renderer->setPosition(tiles[j]->getX()*tileSize,tiles[j]->getY()*tileSize);
            }

            //If the layer is a physic layer(start by phy_), add a collider to the gameobject.
            if(_level.getLayerByIndex(i).getName().find("phy_") != std::string::npos)
            {
                BoxCollider2D* boxCollider = (BoxCollider2D*)tile->addComponentOfType(ComponentType::E_BoxCollider2D);

                if(renderer != nullptr)
                {
                    boxCollider->getRect().top = tiles[j]->getY()*tileSize;
                    boxCollider->getRect().left = tiles[j]->getX()*tileSize;
                    boxCollider->getRect().width = tileSize;
                    boxCollider->getRect().height = tileSize;

                    CollisionSystem::getInstance()->addCollider(boxCollider);
                }
            }

            _gameObjects.push_back(tile);
        }
    }

    //Load all the gameobjects of the level
    loadGameObjects(this->_level.getName());

    std::cout << "Level loaded" << std::endl;
}

void PlayGround::loadGameObjects(const std::string& levelName)
{
    //Get the folder path
    const std::string path = "Ressources/GameObjects/"+levelName;

    //Load the entities based on the entities.txt file that lists them all
    std::string line;
    std::ifstream out(path+"/entities.txt");
    while(getline(out, line))
    {
        GameObject* g = GobSerialization::loadGobFromFile(path+"/"+line);
        //Some gameobjects need to be put aside, so we do that
        if(line == "Player.json")
        {
            this->_player = g;
        }
        else if(line == "Projectile.json")
        {
            this->_projectile = g;
            this->_projectile->deactivate();
        }
        else if(line=="Enemy.json")
        {
            this->_enemy = g;
        }
        this->_gameObjects.push_back(g);
    }
    out.close();
}

PlayGround::~PlayGround()
{
    for (int i = 0; i < _gameObjects.size(); i++)
    {
        if(_gameObjects[i] != nullptr)
        {
            delete _gameObjects[i];
        }
    }

    _gameObjects.clear();
}
