#include "MainMenu.hpp"

void MainMenu::initializeFrame(sf::RenderWindow& window)
{
    //Create the buttons then set their positions
    _playButton = new Button(0,0,BUTTON_WIDTH,BUTTON_HEIGHT,"Ressources/img/button.png","Play");
    _aboutUsButton = new Button(0,100,BUTTON_WIDTH,BUTTON_HEIGHT,"Ressources/img/button.png","About us");
    _exitButton = new Button(0,200,BUTTON_WIDTH,BUTTON_HEIGHT,"Ressources/img/button.png","Exit");

    _playButton->setPos(window.getSize().x/2-BUTTON_WIDTH/2,window.getSize().y/2-BUTTON_HEIGHT/2-125);
    _aboutUsButton->setPos(window.getSize().x/2-BUTTON_WIDTH/2,window.getSize().y/2-BUTTON_HEIGHT/2);
    _exitButton->setPos(window.getSize().x/2-BUTTON_WIDTH/2,window.getSize().y/2-BUTTON_HEIGHT/2+125);
}

void MainMenu::executeFrame(sf::RenderWindow& window, const sf::Time& deltaTime)
{
    //Draw the buttons then display the scene
    _playButton->draw(window);
    _aboutUsButton->draw(window);
    _exitButton->draw(window);

    window.display();

    //Wait for a result
    MenuResult result = getResult(window);

    //Do the appropriate action based on the result
    switch(result)
    {
    case MenuResult::play:
        Game::getInstance()->changeState(GameState::Playing);
        break;
    case MenuResult::aboutUs:
        Game::getInstance()->changeState(GameState::ShowingAboutUs);
        break;
    case MenuResult::exitGame:
        Game::getInstance()->changeState(GameState::Exiting);
        break;
    }

    return;
}

MenuResult MainMenu::getResult(sf::RenderWindow& window)
{
    //While no button has been clicked ...
    while(true)
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            switch(event.type)
            {
            case sf::Event::MouseButtonPressed:
                {
                    MenuResult res = handleClick(event.mouseButton.x,event.mouseButton.y);
                    if(res != none)
                    {
                        return res;
                    }
                    break;
                }
            case sf::Event::Closed:
                return MenuResult::exitGame;
                break;
            case sf::Event::Resized:
                sf::View playerView;
                playerView.reset(sf::FloatRect(0,0,window.getSize().x,window.getSize().y));
                window.setView(playerView);
                window.clear(sf::Color(0,128,128));
                _playButton->setPos(window.getSize().x/2-BUTTON_WIDTH/2,window.getSize().y/2-BUTTON_HEIGHT/2-125);
                _aboutUsButton->setPos(window.getSize().x/2-BUTTON_WIDTH/2,window.getSize().y/2-BUTTON_HEIGHT/2);
                _exitButton->setPos(window.getSize().x/2-BUTTON_WIDTH/2,window.getSize().y/2-BUTTON_HEIGHT/2+125);
                _playButton->draw(window);
                _aboutUsButton->draw(window);
                _exitButton->draw(window);

                window.display();
                break;
            }
        }
    }
}

MenuResult MainMenu::handleClick(const int x,const int y)
{
    if(_playButton->getRect()->intersects(sf::IntRect(x,y,1,1)))
    {
        return MenuResult::play;
    }
    else if(_aboutUsButton->getRect()->intersects(sf::IntRect(x,y,1,1)))
    {
        return MenuResult::aboutUs;
    }
    else if(_exitButton->getRect()->intersects(sf::IntRect(x,y,1,1)))
    {
        return MenuResult::exitGame;
    }

    return none;
}

MainMenu::~MainMenu()
{
    delete _playButton;
    delete _aboutUsButton;
    delete _exitButton;
}
