//! The PlayGround scene.
/*! This scene is responsible to loading the level and letting the players play.
    We can load any level in it as long as it come from a json exported from tiled.
 */
#ifndef PLAYGROUND_HPP_INCLUDED
#define PLAYGROUND_HPP_INCLUDED

#include <vector>
#include "IScene.hpp"
#include <iostream>
#include "../../modele/GameLogic/Game.hpp"
#include "../../modele/ComponentSystem/GameObject/GameObject.hpp"
#include "../../modele/ComponentSystem/Components/RendererComponent.hpp"
#include "../../modele/ComponentSystem/Components/BoxCollider2D.hpp"
#include "../../modele/Level/Level.hpp"
#include "../../util/Serialization/LevelSerialization.hpp"
#include "../../util/TilesetManager.hpp"
#include "../../modele/GameLogic/CollisionSystem.hpp"
#include "../../util/Serialization/GobSerialization.hpp"

#define VIEW_HEIGHT 512.0f

class PlayGround : public IScene
{
public:
    //! Constructor.
    /*! Does not do anything for this class. */
    PlayGround() {};
    //! Initialize the scene.
    /*! Initialize the scene by loading the level and gameobjects. */
	void initializeFrame(sf::RenderWindow& window);
    //! Draw the scene on the screen.
    /*! Draw the scene on the screen and update the gameobjects in it.*/
    void executeFrame(sf::RenderWindow& window, const sf::Time& deltaTime);
    //! Destructor.
    /*! Destroy all the gameobjects of the scene. */
	virtual ~PlayGround();
private:
    //! Initialize a level.
    /*! Initialize the given level, loading it from the json and creating all the gameobjects. */
    void initLevel(const std::string& path);
    //! Load the entities.
    /*! Load all the entities of a given level from their json files. */
    void loadGameObjects(const std::string& levelName);
    //! The gameobjects vector.
    /*! Hold all the gameobjects used by the scene (level + entities). */
	std::vector<GameObject*> _gameObjects;
    //! The current level.
    /*! The object representation of a level. Used as transition between json and gameobjects */
	Level _level;
    //! The player gameobject.
    /*! A pointer to the player gameobject, used to move the view around the scene.\n
    Note that since it is destroyed along with the other gameobjects in the vector, destroying this pointer is useless */
	GameObject* _player;
    //! The projectile gameobject.
    /*! A pointer to the projectile gameobject, used to set it to the playercontroller.\n
    Note that since it is destroyed along with the other gameobjects in the vector, destroying this pointer is useless */
	GameObject* _projectile;
    //! The player view.
    /*! The player view define what section of the screen is shown to the player. */
    sf::View playerView;
    //! The player renderer.
    /*! A pointer to the player renderer is held to update the view accordingly to the player position. */
    RendererComponent* renderer;
    //! The enemy gameobject.
    /*! The enemy gameobject is used to verify if the enemy is deactivated, which mean that we won. */
    GameObject* _enemy;
};

#endif // PLAYGROUND_HPP_INCLUDED
