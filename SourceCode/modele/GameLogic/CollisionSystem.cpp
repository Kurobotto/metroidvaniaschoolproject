#include "CollisionSystem.hpp"

CollisionSystem* CollisionSystem::_instance = nullptr;

bool CollisionSystem::isColliding(BoxCollider2D& collider)
{
    bool collision = false;

    for(int j = 0; j < _colliders.size(); j++)
    {
        if(_colliders[j]->isActivated() && collider.getRect().intersects(_colliders[j]->getRect()) && &collider != _colliders[j])
        {
            ((GameObject*)collider.getGameObject())->onCollision(*_colliders[j]);
            ((GameObject*)_colliders[j]->getGameObject())->onCollision(collider);
            collision = true;
        }
    }

    return collision;
}

void CollisionSystem::reset()
{
    _colliders.clear();
}

bool CollisionSystem::isPointColliding(float x, float y)
{
    bool collision = false;

    sf::FloatRect collider(x,y,1,1);

    for(int j = 0; j < _colliders.size(); j++)
    {
        if(collider.intersects(_colliders[j]->getRect()))
        {
            collision = true;
        }
    }

    return collision;
}
