//! Define the Collision System
/*! Contains all the logic that defines the Collision system. */

#ifndef COLLISIONSYSYSTEM_HPP_INCLUDED
#define COLLISIONSYSYSTEM_HPP_INCLUDED

#include <vector>
#include "../ComponentSystem/GameObject/GameObject.hpp"
#include "../ComponentSystem/Components/BoxCollider2D.hpp"

class CollisionSystem
{
public:
    //! Instance getter
    /*! Return the Singleton instance of the CollisionSystem class, or create a new one if it does not exist yet.*/
    static CollisionSystem* getInstance()
    {
        if(_instance == nullptr)
        {
            _instance = new CollisionSystem();
        }

        return _instance;
    }
    //! Adds a 2D box collider
    /*!  Adds a 2D box collider. */
    void addCollider(BoxCollider2D* collider)
    {
        _colliders.push_back(collider);
    }
    //! Collider state
    /*! Check all the game objects and returns true if a game object is colliding another. */
    bool isColliding(BoxCollider2D& collider);
    //! Point colliding state
    /*! check if this collider is colliding another. */
    bool isPointColliding(float x, float y);
    //! Reset
    /*! Resets the collision system. */
    void reset();
    //! Destructor
    /*! Prevents memory leaks. */
    virtual ~CollisionSystem() {};
private:
    //! Constructor
    /*! The constructor is private to respect the singleton pattern. */
    CollisionSystem() {};
    //! Collision system Instance;
    /*! The collision system instance. */
    static CollisionSystem* _instance;
    //! Colliders
    /*! The vector of colliders. */
    std::vector<BoxCollider2D*> _colliders;
};

#endif
