#include "Level.hpp"

void Level::addLayer(const std::vector<Tile*>& tiles, const std::string& name)
{
  _layers.push_back(new Layer(tiles,name));
}

Level::~Level()
{
    for(int i = 0; i < this->_layers.size(); i++)
    {
        delete this->_layers[i];
    }
}

Level& Level::operator=(const Level& rhs)
{
    if (this == &rhs) return *this;

    for(int i = 0; i < rhs._layers.size(); i++)
    {
        this->_layers.push_back(new Layer(*rhs._layers[i]));
    }

    this->_name = rhs._name;
    this->_height=rhs._height;
    this->_width=rhs._width;
    this->_tileSize=rhs._tileSize;
    return *this;
}

Level::Level(const Level& other)
{
    for(int i = 0; i < other._layers.size(); i++)
    {
        this->_layers.push_back(new Layer(*other._layers[i]));
    }

    this->_name = other._name;
    this->_height=other._height;
    this->_width=other._width;
    this->_tileSize=other._tileSize;
}

std::string Level::str()
{
    std::stringstream ss;
    ss << "name : " << this->_name << std::endl <<
    "(height,width) : (" << this->_height << "," << this->_width << ")" << std::endl <<
    "TileSize : " << this->_tileSize << std::endl << "Layers : " << std::endl;

    int totalCase = 0;

    for(int i = 0; i < _layers.size();i++)
    {
        ss << "    " << _layers[i]->getName() << std::endl;

        ss << "Cases : " << _layers[i]->getTiles().size() << std::endl;
        totalCase+= _layers[i]->getTiles().size();
    }

    ss<< "Total cases : " << totalCase << std::endl;
    return ss.str();
}
