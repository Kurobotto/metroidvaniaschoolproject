//! Defines the TileSet
/*! Hold the logic that defines a tile set. */

#ifndef TILESET_HPP_INCLUDED
#define TILESET_HPP_INCLUDED

#include <iostream>

class Tileset
{
public:
    //! Constructor
    /*! Instantiate the tile set.*/
    Tileset(const int firstGid = 0, const std::string& path = "") : _firstGid(firstGid), _path(path) {};
    //! First Gid Getter
    /*! Gets the first Gid */
    int getFirstGid() {return _firstGid;};
    //! Path getter
    /*! Gets the path of the tile set.*/
    std::string getPath() {return _path;};
private:
    //! First Gid
    /*! The first Gid of the tile set. */
    const int _firstGid;
    //! Tile set path
    /*! The tile set Path. */
    const std::string _path;
};

#endif // TILESET_HPP_INCLUDED
