#include "Layer.hpp"

Layer::~Layer()
{
    for(int i = 0; i < this->_tiles.size(); i++)
    {
        delete this->_tiles[i];
    }
}

Layer::Layer(const Layer& other) : _name(other._name)
{
    for(int i = 0; i < other._tiles.size(); i++)
    {
        this->_tiles.push_back(new Tile(*other._tiles[i]));
    }
}

Layer& Layer::operator=(const Layer& rhs)
{
    if (this == &rhs) return *this;
    this->_name = rhs._name;
    for(int i = 0; i < rhs._tiles.size(); i++)
    {
        this->_tiles.push_back(new Tile(*rhs._tiles[i]));
    }
    return *this;
}
