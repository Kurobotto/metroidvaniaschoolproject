//! Define a tile
/*! It holds the logic that define a tile. */
#ifndef TILE_HPP_INCLUDED
#define TILE_HPP_INCLUDED

class Tile
{
public:
    //! Constructor
    /*! Instantiate the tile. */
    Tile(const int x = 0, const int y = 0, const int gid = 0) : _x(x), _y(y), _gid(gid) {};
    //! Destructor
    /*! Prevents memory leaks. */
    virtual ~Tile() {};
    //! Copy Constructor
    /*! Create a new Instance by copying another. */
    Tile(const Tile& other) : _x(other._x), _y(other._y), _gid(other._gid) {};
    //! Operator =
    /*! Redefine the equal operator to adapt it for this class. */
    Tile& operator=(const Tile& rhs)
    {
        if (this == &rhs) return *this;
        this->_x = rhs._x;
        this->_y = rhs._y;
        this->_gid = rhs._gid;
        return *this;
    }
    //! X axis getter
    /*! Gets the X axis. */
    int getX() const {return _x;};
    //! Y axis getter
    /*! Gets the Y axis. */
    int getY() const {return _y;};
    //! Gud Getter
    /*! Tells if you git gud enough */
    int getGid() const {return _gid;};
private:
    //! X axis
    /*! The X axis. */
    int _x;
    //! Y axis
    /*! The Y axis. */
    int _y;
    //! Gid
    /*! The Gid. */
    int _gid;
};

#endif // TILE_HPP_INCLUDED
