//! Component that handle the animations of the game.
/*!
    It holds the entire logic that decides which animation to play and synchronize
    the animations depending on the frames
*/
#ifndef ANIMATOR_HPP_INCLUDED
#define ANIMATOR_HPP_INCLUDED

#include "IComponent.hpp"

#define FRAME_BETWEEN_ANIM 12
#define IDLE_FRAME_NUMBER 4
#define RUNNING_FRAME_NUMBER 8
#define JUMPING_FRAME_NUMBER 2


class Animator : public IComponent
{
public:
    //! Constructor
    /*! Instantiate the component. */
    Animator(IGameObject* owner): IComponent(owner,ComponentType::E_Animator){};
    //! Destructor
    /*! Prevents memory leaks */
    virtual ~Animator(){};
    //! Copy Constructor
    /*! Create a new instance by copying another. */
    Animator(const Animator& other);
    //! Operator =
    /*! Redefine the equal operator to adapt it for this class */
    Animator& operator=(const Animator& rhs);
    //! Clone
    /*! Clone the instance by creating a new instance that contains the same values of the original one. */
    IComponent* clone(IGameObject* owner);
    //! On Collision Event
    /*! Handle the collision event. */
    void onCollision(IComponent& other){};
    //! Receive messages
    /*! Receive the physic state and interprets it to decide which animations to play.*/
    void receiveMessage(const std::string& message);
    //! Start
    /*! Handle Start event */
    void start() {};
    //! Update
    /*! Handle Update event */
    void update(const sf::Time& deltaTime);
private:
    //! The Remaining frames
    /*! The number of frames remaining before the next animation frame. */
    int _remainingFrame = FRAME_BETWEEN_ANIM;
    //! The Current Animation
    /*! The current animation playing. */
    Animations _currentAnim = Animations::idle;
    //! The Current Index
    /*! The Index of the current playing animation. */
    int _currentIndex = 0;
    //! The current animation
    /*! The number of frames of the current animation */
    int _currentAnimFrameNbr;
};

#endif // ANIMATOR_HPP_INCLUDED
