//! Component that define the physic state of an entity
/*!
    It holds the component logic that define entities physic state.
*/

#ifndef PHYSICSBODY_HPP_INCLUDED
#define PHYSICSBODY_HPP_INCLUDED
#include "IComponent.hpp"
#include "RendererComponent.hpp"
#include "BoxCollider2D.hpp"
#include "../../GameLogic/CollisionSystem.hpp"
#include "../GameObject/GameObject.hpp"
#define MAX_VELOCITY_Y 5
#define MAX_VELOCITY_X 50
#define GRAVITY 130

class PhysicsBody :
	public IComponent
{
public:
    //! Constructor
    /*! Instantiate the component */
	PhysicsBody(IGameObject* owner) : IComponent(owner,ComponentType::E_PhysicsBody) {};
	//! Destructor
	/*! Prevents memory leaks. */
	~PhysicsBody();
	//! Copy Constructor
	/*! Create a new instance by copying another. */
	PhysicsBody(const PhysicsBody& other);
	//! Operator =
	/*! Redefine the equal operator to adapt it for this class */
	PhysicsBody& operator=(const PhysicsBody& rhs);
	//! Clone
	/*! Clone the instance by creating a new one that contains the same values. */
	IComponent* clone(IGameObject* owner);

    //! On receive message event
    /*! Handle message reception. */
	void receiveMessage(const std::string& message);
	//! On Start event
	/*! Handle start event */
	void start();
	//! On update event
	/*! Handle update event. */
	void update(const sf::Time& deltaTime);
	//! On Collision event
	/*! Handle collision event */
	void onCollision(IComponent& other) {};
	//! Resolve Collision Event
	/*! Event that will resolve some collision issues when an entity is stuck. */
	void resolveCollision(bool xAxis);
	//! Grounded Getter
	/*! Grounded boolean getter that will return if an entity is grounded or not. */
    bool isGrounded(){return _grounded;};
    //! Add force to an entity
    /*! to influe on entity's velocity. */
    void addForce(float xForce, float yForce)
    {
        _velocityX += xForce;
        _velocityY += yForce;
    }

private:
    //! Translate All
    /*! Translate both PhysicsBody and Renderer. */
	void translateAll(float x, float y);
	//! X axis velocity.
	/*! Defines x axis velocity. */
    float _velocityX = 0;
    //! Y axis velocity.
    /*! Defines y axis velocity. */
    float _velocityY = 0;
    //! Grounded entity
    /*! Boolean that defines if an entity is grounded or not. */
	bool _grounded = false;
	//! Renderer
	/*! Used to link the renderer to the PhysicsBody. */
    RendererComponent* _renderer = nullptr;
    //!Collider
    /*! Used to link the collider to the PhysicsBody. */
	BoxCollider2D* _collider = nullptr;

};

#endif
