//! Component Interface
/*! Interface that specify the method behavior that each component should implement. */

#ifndef ICOMPONENT_HPP_INCLUDED
#define ICOMPONENT_HPP_INCLUDED
#include <sstream>
#include <iostream>
#include <istream>
#include "SFML/Window.hpp"
#include "SFML/Graphics.hpp"
#include "../GameObject/IGameObject.hpp"
#include "../../GameLogic/Enumerators.hpp"

class IComponent
{
public:
    //! Clone
    /*! Abstract Clone method handler */
	virtual IComponent* clone(IGameObject* owner) = 0;
    //! Start event
    /*! Abstract Start event.
        Start should be used to initialize members. */
    virtual void start() = 0;
    //! Update
    /*! Abstract Update event
        Update should hold the component logic. */
	virtual void update(const sf::Time& deltaTime)
	{
		if(!_isActivated)
		{
			return;
		}
	};
	//! On Collision Event
	/*! Abstract onCollision Event.
        onCollision should hold the action to perform when the gob enter in collision. */
	virtual void onCollision(IComponent& other) = 0;
	//! On Receive message event
	/*! Let the component receive messages from other components and interprets them */
	virtual void receiveMessage(const std::string& message) = 0;
	//! Component Type Getter
	/*! Getter that return the component type.*/
	virtual ComponentType getType() { return _type; };
	//! Deactivate component
	/*! If the component is activated it will deactivate it.*/
	virtual void deactivate() { _isActivated = false; };
	//! Activate component
	/*! Activate the component if it's not already.*/
	virtual void activate() { _isActivated = true; };
	//! Activation Getter
	/*! Getter of the component state. */
	virtual bool isActivated() { return _isActivated; };
	//! Game object getter
	/*! Get the game object.*/
	virtual IGameObject* getGameObject() {return _gameobject; }
	//! Destructor
	/*! Prevents memory leaks*/
	virtual ~IComponent() { };

protected:
    //! Constructor
    /*! The constructor is protected to be used only by subclasses */
	IComponent(IGameObject* owner, const ComponentType type) : _gameobject(owner), _type(type) {};
	//! Copy Constructor
	/*! Create a new instance by copying another. */
    IComponent(const IComponent& other);
    //! Operator =
    /*! Redefine the equal operator to adapt it for this class */
    IComponent& operator=(const IComponent& rhs);
    //! A GameObject Instance
    /*! to link components to a game object. */
	IGameObject* _gameobject;
	//! Type
	/*! To define the type of each component.  */
	ComponentType _type;
	//! State of the component
	/*! boolean that define the state of the component. */
	bool _isActivated = true;

};

#endif
