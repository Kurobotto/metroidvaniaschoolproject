//! Component that represents the Collisions of the game.
/*!
  It holds the entire logic that handle the collision between entities.
*/

#ifndef BOXCOLLIDER2D_HPP_INCLUDED
#define BOXCOLLIDER2D_HPP_INCLUDED

#include "IComponent.hpp"

class BoxCollider2D : public IComponent
{
    public:
        //! Constructor
        /*! Instantiate the component*/
        BoxCollider2D(IGameObject* owner) : IComponent(owner,ComponentType::E_BoxCollider2D){}
        //! Destructor
        /*! Prevents memory leaks. */
        virtual ~BoxCollider2D() {};
        //! Copy Constructor
        /*! Create a new instance by copying another. */
        BoxCollider2D(const BoxCollider2D& other);
        //! Operator =
        /*! Redefine the equal operator to adapt it for this class */
        BoxCollider2D& operator=(const BoxCollider2D& rhs);
        //! Clone
        /*! Clone the instance by creating a new instance that contains the same values of the original one. */
        IComponent* clone(IGameObject* owner);
        //! Rectangle Getter
        /*! Gets the collision rectangle */
        sf::FloatRect& getRect() { return _rect; }
        //! Rectangle Setter
        /*! Sets the Collision Rectangle */
        void setRect(sf::FloatRect val) { _rect = val; }
        //! Translates the position of the collider.
        /*! Translate the position of the x and y value. */
        void translate(float x, float y);
        //! Collision Trigger Getter
        /*! Getter of the trigger state */
        bool isTrigger() { return _isTrigger; }
        //! Collision Trigger Setter
        /*! Setter of the trigger */
        void setIsTrigger(bool val) { _isTrigger = val; }
        //! On collision event
        /*! Handle the collision event */
        void onCollision(IComponent& other) {};
        //! Set the position of the collision rectangle
        /*! Set the position of the collision rectangle by changing the position of the x and y*/
        void setPosition(float x,float y){
            this->_rect.left = x;
            this->_rect.top = y;
        };
        //! Receive messages
        /*! Handle message reception. */
        void receiveMessage(const std::string& message) {};
        //! Start event
        /*! Handle Start Event */
        void start(){};
        //! Update event
        /*! Handle Update Event */
        void update(const sf::Time& deltaTime);

    private:
        //! The collision rectangle
        /*! The collision rectangle sized by floating numbers */
        sf::FloatRect _rect;
        //! Collision trigger
        /*!  Boolean that react when a collision happen between two entities */
        bool _isTrigger;
};

#endif // BOXCOLLIDER2D_HPP_INCLUDED

