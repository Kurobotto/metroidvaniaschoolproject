#include "RendererComponent.hpp"

RendererComponent::RendererComponent(const RendererComponent& other): IComponent(other)
{
    this->_position = other._position;
    this->_rotation = other._rotation;
    this->_scale = other._scale;
    this->_texture = other._texture;
    this->_sprite = other._sprite;
}

RendererComponent& RendererComponent::operator=(const RendererComponent& rhs)
{
    if (this == &rhs) return *this;

    IComponent::operator=(rhs);

    this->_position = rhs._position;
    this->_rotation = rhs._rotation;
    this->_scale = rhs._scale;
    this->_texture = rhs._texture;
    this->_sprite = rhs._sprite;

    return *this;
}

IComponent* RendererComponent::clone(IGameObject* owner)
{
    RendererComponent* comp = new RendererComponent(*this);
    comp->_gameobject = owner;
    return comp;
}

void RendererComponent::receiveMessage(const std::string& message)
{
    std::istringstream iss(message);
    std::vector<std::string> results(std::istream_iterator<std::string>{iss},
    std::istream_iterator<std::string>());

    std::string type = results[0];

    if(type == "TranslateAll")
    {
        float valuex = std::stof(results[1]);
        float valuey = std::stof(results[2]);
        translate(valuex,valuey);
    }
    else if(type == "ChangeAnim")
    {
        int xIndex = std::stoi(results[1]);
        int yIndex = std::stoi(results[2]);
        int mod = _dir == -1 ? 1 : 0;
        this->_sprite.setTextureRect(sf::IntRect(xIndex*_width+mod*_width,yIndex*_height,_dir*_width,_height));
    }
    else if(type == "ChangeDir")
    {
        int dir = std::stoi(results[1]);

        _dir = dir;
    }
}

void RendererComponent::update(const sf::Time& deltaTime)
{
    IComponent::update(deltaTime);
}

void RendererComponent::setPosition(const float x,const float y)
{
    _position.x = x;
    _position.y = y;

    _sprite.setPosition(x,y);
}

void RendererComponent::setRotation(const float r)
{
    _rotation = r;

    _sprite.setRotation(_rotation);
}

void RendererComponent::setScale(const float x,const float y)
{
    _scale.x = x;
    _scale.y = y;

    _sprite.setScale(x,y);
}

void RendererComponent::setTexture(const std::string& path, const int overrideWidth, const int overrideHeight)
{
    if(_texture.loadFromFile(path))
    {
        _sprite.setTexture(_texture);

        _width = overrideWidth;
        _height = overrideHeight;
        this->_sprite.setTextureRect(sf::IntRect(0,0,_width,_height));

        if(overrideWidth != 0)
        {
            _sprite.setScale(overrideWidth/_sprite.getLocalBounds().width,_sprite.getScale().y);
        }

        if(overrideHeight != 0)
        {
            _sprite.setScale(_sprite.getScale().x,overrideHeight/_sprite.getLocalBounds().height);
        }
        //_sprite.setOrigin(_width/2,_height/2);
    }
    else
    {
        std::cout << "couldn't load image" << std::endl;
    }
}

void RendererComponent::setSprite(const sf::Sprite& sprite, const int overrideWidth, const int overrideHeight)
{
    _sprite = sprite;

    if(overrideWidth != 0)
    {
        _sprite.setScale(overrideWidth/_sprite.getLocalBounds().width,_sprite.getScale().y);
    }

    if(overrideHeight != 0)
    {
        _sprite.setScale(_sprite.getScale().x,overrideHeight/_sprite.getLocalBounds().height);
    }
}

void RendererComponent::translate(const float x, const float y)
{
    this->_position.x += x;
    this->_position.y += y;

    _sprite.setPosition(_position.x,_position.y);
}
