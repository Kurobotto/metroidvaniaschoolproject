#include "EnemyController.hpp"

EnemyController::EnemyController(const EnemyController& other):IComponent(other)
{
}

EnemyController& EnemyController::operator=(const EnemyController& rhs)
{
    if (this == &rhs) return *this;
    IComponent::operator=(rhs);
    return *this;
}

IComponent* EnemyController::clone(IGameObject* owner)
{
    EnemyController* comp = new EnemyController(*this);
    comp->_gameobject = owner;
    return comp;
}

void EnemyController::start()
{
}

void EnemyController::update(const sf::Time& deltaTime)
{
    IComponent::update(deltaTime);
    std::stringstream ss;

     ss << "AddForce " << MOVE_FORCE_ENEMY*_dir <<" "<< 0;
     this->_gameobject->sendMessage(ss.str());
}

void EnemyController::onCollision(IComponent& other)
{
    Projectile* proj = ((Projectile*)((GameObject*)other.getGameObject())->getComponentOfType(ComponentType::E_Projectile));

    if(proj != nullptr)
        this->getGameObject()->deactivate();

    this->_dir *= -1;
    std::stringstream sss;
    sss << "ChangeDir " << _dir;
    this->_gameobject->sendMessage(sss.str());
}
