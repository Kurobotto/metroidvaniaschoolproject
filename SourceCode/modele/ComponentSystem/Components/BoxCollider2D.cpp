#include "BoxCollider2D.hpp"

BoxCollider2D::BoxCollider2D(const BoxCollider2D& other): IComponent(other)
{
    this->_rect = other._rect;
    this->_isTrigger = other._isTrigger;
}

BoxCollider2D& BoxCollider2D::operator=(const BoxCollider2D& rhs)
{
    if (this == &rhs) return *this;

    IComponent::operator=(rhs);

    this->_rect = rhs._rect;
    this->_isTrigger = rhs._isTrigger;
    return *this;
}

void BoxCollider2D::update(const sf::Time& deltaTime)
{
    IComponent::update(deltaTime);
}

IComponent* BoxCollider2D::clone(IGameObject* owner)
{
    BoxCollider2D* comp = new BoxCollider2D(*this);
    comp->_gameobject = owner;
    return comp;
}

void BoxCollider2D::translate(float x, float y)
{
    this->_rect.left += x;
    this->_rect.top += y;
}
