#include "Projectile.h"


 Projectile::Projectile(const Projectile& other):IComponent(other)
{

}

Projectile::~Projectile()
{
    //dtor
}

Projectile& Projectile::operator=(const Projectile& rhs)
{
    if(this == &rhs) return *this;

    IComponent::operator=(rhs);

}

void Projectile::onCollision(IComponent& other)
{
    this->_gameobject->deactivate();
}



IComponent* Projectile::clone(IGameObject* owner)
{
    Projectile* comp = new Projectile(*this);
    comp->_gameobject = owner;
    return comp;
}


void Projectile::start()
{
}

void Projectile::update(const sf::Time& deltaTime)
{
    IComponent::update(deltaTime);
    std::stringstream ss;

     ss << "AddForce " << MOVE_FORCE_PROJ*_dir <<" "<< 0;
     this->_gameobject->sendMessage(ss.str());
}
