#include "IComponent.hpp"

IComponent::IComponent(const IComponent& other)
{
    this->_gameobject = other._gameobject;
    this->_isActivated = other._isActivated;
    this->_type= other._type;
}

IComponent& IComponent::operator=(const IComponent& rhs)
{
    if (this == &rhs) return *this;

    this->_gameobject = rhs._gameobject;
    this->_isActivated = rhs._isActivated;
    this->_type= rhs._type;

    return *this;
}