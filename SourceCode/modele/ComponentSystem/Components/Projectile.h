#ifndef PROJECTILE_H
#define PROJECTILE_H
#include "IComponent.hpp"
#include "../GameObject/IGameObject.hpp"


#define MOVE_FORCE_PROJ 700

class Projectile : public IComponent
{
    public:
        Projectile(IGameObject* owner): IComponent(owner,ComponentType::E_Projectile) {};
        virtual ~Projectile();
        Projectile(const Projectile& other);
        Projectile& operator=(const Projectile& rhs);
        IComponent* clone(IGameObject* owner);
        void receiveMessage(const std::string& message){};
        void onCollision(IComponent& other);
        void start();
        void update(const sf::Time& deltaTime);
        void setDir(int dir){this->_dir = dir;}

    protected:

    private:
        int _dir = 1;
};

#endif // PROJECTILE_H
