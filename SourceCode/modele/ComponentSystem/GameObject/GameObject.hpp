//! Represents the entities of the game.
/*! Holds the entire logic of an entity and behavior with its components. */
#ifndef GAMEOBJECT_HPP_INCLUDED
#define GAMEOBJECT_HPP_INCLUDED
#include <array>
#include "IGameObject.hpp"
#include "../Components/IComponent.hpp"
#include "../Components/PlayerController.h"
#include "../Components/RendererComponent.hpp"
#include "../Components/PhysicsBody.hpp"
#include "../Components/BoxCollider2D.hpp"
#include "../Components/Animator.hpp"
#include "../Components/EnemyController.hpp"

class GameObject : public IGameObject
{
public:
    //! Constructor
    /*! Instantiate the game object. */
	GameObject(const std::string& name) : _name(name) {};
	//! Destructor
	/*! Prevents memory leaks. */
	virtual ~GameObject();
	//! Copy Constructor
	/*! Create a new instance by creating a new instance that contains the same values. */
	GameObject(const GameObject& other);
    //! Operator =
    /*! Redefine the equal operator to adapt it for this class */
	GameObject& operator=(const GameObject& rhs);
	//! Start all components event
	/*! Change the state of all components of the game object. */
	void start();
	//! Activate all components
	/*! Activate all the components of the game object. */
    void activate();
    //! Deactivate all components
    /*! Deactivates all components of the game object.*/
    void deactivate();
    //! Send message
    /*! Broadcasts message to the components.*/
	void sendMessage(const std::string& message) const;
	//! Component type Getter
	/*! Get the type of the component in parameter. */
	IComponent* getComponentOfType(const ComponentType type);
	//! Add a component
	/*! Add the component in parameter to the game object*/
	IComponent* addComponentOfType(const ComponentType type);
	//! Remove a component
	/*! Remove the component in parameter from the game object. */
	bool removeComponentOfType(const ComponentType type);
	//! On collision event
	/*! Trigger the onCollision event of all the components linked to the game object.*/
	void onCollision(IComponent& other);
	//! Is game object active
	/*! Boolean that indicates if the Game object is activated. */
	bool isActivated(){return this->_activated;}

	//! Update event.
	/*! Update event, update deltatime and return a sprite when the gameobject have a renderer on it,
        so that the caller can draw it.
    */
	sf::Sprite* update(const sf::Time& deltaTime);
private:
    //! Linked components vector
    /*! Vector that contains the components of the game object.*/
	std::vector<IComponent*> _components;
	//! The Game object name.
	/*! The game object's name.*/
	std::string _name;
};

#endif
